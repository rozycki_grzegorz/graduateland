<?php

namespace Model;

class Job
{
    // FYI setting properties as public has some benefits:
    //  - simple access,
    //  - built in array casting and serialization support
    // A readonly modifier would be great though.
    public $url = '';
    public $title = '';
    public $description = '';
    public $experienced = false;
    public $years = 0;

    public static function makeFromObject(object $input): Job
    {
        $instance = new static();
        $instance->url = $input->url ?? '';
        $instance->title = $input->title ?? '';
        $instance->description = $input->description ?? '';
        $instance->experienced = $input->experienced ?? false;
        $instance->years = $input->years ?? 0;

        return $instance;
    }
}
