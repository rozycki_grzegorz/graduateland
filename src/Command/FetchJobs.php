<?php

namespace Command;

use DOMElement;
use Model\Job;
use Service\Writer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

class FetchJobs extends Command
{
    const NAME = 'app:fetch-jobs';

    protected static $defaultName = self::NAME;

    /** @var HttpClientInterface $client */
    protected $client;

    public function __construct()
    {
        parent::__construct();
        $this->client = HttpClient::create();
    }

    public static function makeWriter(InputInterface $input): Writer
    {
        switch (true) {
            case $input->getOption('csv'):
                return new Writer\Csv();
            case $input->getOption('json'):
                return new Writer\Json();
            case $input->getOption('var_export'):
                return new Writer\VarExport();
            default:
                return new Writer\Printr();
        }
    }

    protected function configure()
    {
        $this
            ->addOption('csv', null, InputOption::VALUE_NONE, 'Write results in CSV format')
            ->addOption('json', null, InputOption::VALUE_NONE, 'Write results in JSON format')
            ->addOption('var_export', null, InputOption::VALUE_NONE, 'Write results in var_export format')
            ->setDescription('Fetches job listing from Spotify')
            ->setHelp('This command fetches all IT jobs in Sweden posted on Spotify and outputs the results to standard output. By default it uses print_r for output formatting.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Writer $writer */
        $writer = self::makeWriter($input);
        $page = 1;

        do {
            [$jobs, $next] = $this->fetchJobListing($page++);

            foreach ($jobs as $job) {
                $writer->write($this->fetchJobDetails($job));
            }
        } while ($next);

        return 0;
    }

    protected function fetchJobListing(int $page): array
    {
        try {
            $response = $this->client->request('POST', 'https://www.spotifyjobs.com/wp-admin/admin-ajax.php', [
                'body' => [
                    'action' => 'get_jobs',
                    'pageNr' => $page,
                    'perPage' => 100,
                    'featuredJobs' => null,
                    'category' => 0,
                    'locations' => 0,
                    'locations[]' => 'sweden',
                ],
            ]);
            $json = json_decode($response->getContent());
            $items = $json->data->items ?? [];
            $next = $json->data->show_pager ?? false;
        } catch (Throwable $e) {
            $items = [];
            $next = false;
        }

        return [$items, $next];
    }

    protected function fetchJobDetails(object $job): Job
    {
        $details = Job::makeFromObject($job);

        try {
            $html = $this->client
                ->request('GET', $details->url)
                ->getContent();
            $crawler = new Crawler($html);
            $details->description = $crawler
                ->filter('.column-inner > div')
                ->first()
                ->text();

            /** @var DOMElement $li */
            foreach ($crawler->filter('.column-inner > ul > li') as $li) {
                preg_match('/(\d+)\s*\+?\s*year/i', $li->textContent, $matches);
                $years = intval($matches[1] ?? 0);

                if ( ! $years) {
                    continue;
                }

                $details->years = $years;
                $details->experienced = $years > 2;
                break;
            }
        } catch (Throwable $e) {
            // ignore
        }

        return $details;
    }
}
