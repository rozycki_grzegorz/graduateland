<?php

namespace Service\Writer;

use Model\Job;
use Service\Writer;

class VarExport implements Writer
{
    public function __construct()
    {
        fprintf(STDOUT, "<?php\nreturn array (\n");
    }

    public function __destruct()
    {
        fprintf(STDOUT, "\n);\n");
    }

    public function write(Job $job)
    {
        fprintf(STDOUT, "%s,\n", var_export((array)$job, true));
    }
}
