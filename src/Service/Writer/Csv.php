<?php

namespace Service\Writer;

use Model\Job;
use Service\Writer;

class Csv implements Writer
{
    public function write(Job $job)
    {
        fputcsv(STDOUT, (array)$job);
    }
}
