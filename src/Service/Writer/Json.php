<?php

namespace Service\Writer;

use Model\Job;
use Service\Writer;

class Json implements Writer
{
    public function __construct()
    {
        fprintf(STDOUT, "[\n");
    }

    public function __destruct()
    {
        fprintf(STDOUT, "]");
    }

    public function write(Job $job)
    {
        fprintf(STDOUT, "%s,\n", json_encode($job));
    }
}
