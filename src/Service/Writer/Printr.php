<?php

namespace Service\Writer;

use Model\Job;
use Service\Writer;

class Printr implements Writer
{
    public function write(Job $job)
    {
        print_r($job);
    }
}
