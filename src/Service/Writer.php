<?php

namespace Service;

use Model\Job;

interface Writer
{
    public function write(Job $job);
}
