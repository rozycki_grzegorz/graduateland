# About the implementation

I decided to implement the task as a Symfony console command.
I didn't use the entire framework (only some basic components) 
as IMO it was too much for this task.

The main loop fetches job ads via the JSON listing endpoint, 
details are scraped from  HTML.

Code is also available via bitbucket repo:
https://bitbucket.org/rozycki_grzegorz/graduateland/src/master/

## How to run it

On Linux it should be enough to invoke
`./main app:fetch-jobs` or `php main app:fetch-jobs`.

By default the results are saved in a CSV file called jobs.csv 
(in current working directory).

### Some ideas on extending this command

- a progress indicator would be nice,
- add a box config for building a phar.
